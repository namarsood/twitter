/**
 * Created by namarsood on 1/22/2016.
 */
var Plugins = require('./Plugins'),
    hapi=require('hapi'),
    CONSTANT=require('./config/constants'),
    server=new hapi.Server(),
    mongoose=require('mongoose'),
    userEndpoints=require('./routes/userRoutes'),
    adminEndpoints=require('./routes/adminRoutes');
mongoose.connect('mongodb://inter_4:inter_4_pwd@localhost/twitter_4');

//mongoose.connect('mongodb://localhost:27017/practice');
server.connection({
    port:CONSTANT.PORT,
    //host:CONSTANT.HOST
});

server.register(Plugins, function (err) {
    if (err) {
        server.error('Error while loading Plugins : ' + err)
    } else {
        server.log('info', 'Plugins Loaded')
    }
});

server.route(userEndpoints);
server.route(adminEndpoints);

server.start(function () {
    server.log('Server running at:', server.info.uri);
});