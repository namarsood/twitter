/**
 * Created by namarsood on 2/16/2016.
 */
var forEach = require('async-foreach').forEach;
var async=require('async');
var arr=[[1,2],[1,3],[1,4]];
var newArr=[];
 // 1st method
//forEach(arr, function(arr1) {
//    ans=0;
//    forEach(arr1,function(item)
//    {
//      ans+=  item;
//    });
//    newArr.push(ans);
//});
 // 2nd method
async.forEach(arr,function(item,callback){
    newArr.push(item[0]+item[1]);
    callback();
},function(){
    console.log(newArr);
})
