/**
 * Created by namarsood on 1/22/2016.
 */

var fs=require('fs');
var joi=require('joi'),
    user=require('../controller/user'),
    queries=require('../service/queries'),
    Token=require('../controller/token'),
    tweets=require('../controller/tweet'),
    infixToPostfix=require('../controller/infixToPostfix'),
    joiError=require('../controller/joiError');
    CONSTANT=require('../config/constants');

module.exports=[{
    method:'POST',
    path:'/user/register',
    config:{
        description: 'register',
        notes: 'register',
        tags: ['api','user'],
        validate:{
            payload:{
                username:joi.string().required(),
                password:joi.string().required(),
                email:joi.string().email().required()
            },
            failAction:function(request,reply,source,error)
            {
                var error=joiError(error);
                reply(error.response).code(error.statusCode);
            }
        },
    response: {
        options: {
            allowUnknown: true
        },
        schema: {
            message: joi.string().required(),
            data: joi.object()
        }
    },
        //plugins: {
        //    'hapi-swagger': {
        //        responses: CONSTANT.RESPONSEMESSAGES
        //    }
        //},
        handler:function(request,reply){
            username=request.payload.username;
            password=request.payload.password;
            email=request.payload.email;
            user.register(username,password,email,function(err,result){
                if(err)
                    reply(err.response).code(err.statusCode);
                else
                    reply(result.response).code(result.statusCode);
            });

        }
    }
},
    {
        method:'GET',
        path:'/verifyEmail/{token}',
        config:{
            handler: function(request, reply) {
                var token=request.params.token;
                Token.verifyToken(token,function(err,result){
                    if(err)
                    reply(err);
                    else
                    reply(result);
                });
            }

        }
    },

    {
        method:'GET',
        path:'/user/getToken/{email}',
        config:{
            description: 'showToken',
            notes: 'showToken',
            tags: ['api','user'],
            validate:{
                params:{
                    email:joi.string(),
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data: joi.object().keys({token:joi.string()})
            }
        },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                Token.getToken(request.params.email,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    }
    ,
    {
        method:'POST',
        path:'/user/Login',
        config:{
            description: 'Login',
            notes: 'Login',
            tags: ['api','user'],
            validate:{
                payload:{
                    email:joi.string().email().required(),
                    password:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.object()
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var password=request.payload.password,
                    email=request.payload.email;
                user.login(password,email,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    }
    ,
    {
        method:'DELETE',
        path:'/user/Logout/{token}',
        config:{
            description: 'Logout',
            notes: 'Logout',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data: joi.object()
            }
        },
            plugins: {
                'hapi-swagger': {
                    responses: CONSTANT.RESPONSEMESSAGES
                }
            },
            handler: function(request, reply) {
                var token=request.params.token;
                user.logout(token,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                })
            }

        }
    },
    {
        method:'PUT',
        path:'/user/Tweet/{tweet}/{token}',
        config:{
            description: 'Tweet',
            notes: 'Tweet',
            tags: ['api','user'],
            validate:{
                params:{
                    tweet:joi.string().required(),
                    token:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.object().keys({tweet:joi.string(),
                    _id:joi.any(),
                    dateCreated:joi.date()
                    })
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var tweet=request.params.tweet
                    token=request.params.token;
                tweets.doTweets(tweet,token,function(err,result){
                    if(err)
                    reply(err.response).code(err.statusCode);
                    else
                    reply(result.response).code(result.statusCode);
                });
            }

        }
    },
    {
        method:'GET',
        path:'/user/Timeline/{token}',
        config:{
            description: 'Timeline',
            notes: 'Timeline',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.array().items(joi.object().keys({
                            _id: joi.any(),
                            userInfo:joi.object().keys({
                                _id:joi.any(),
                                username: joi.string(),
                                email: joi.string().email()}),
                            tweet:joi.string(),
                            dateCreated:joi.date()
                }))
            }},
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token;
                tweets.Timeline(token,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    },
    {
        method:'GET',
        path:'/user/Profile/{token}',
        config:{
            description: 'Profile',
            notes: 'Profile',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.array().items(joi.object().keys({
                        _id: joi.any(),
                        userInfo:joi.object().keys({
                            _id:joi.any(),
                            username: joi.string(),
                            email: joi.string().email()}),
                        tweet:joi.string(),
                        dateCreated:joi.date()
                    }))
                }},
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token;
                tweets.Profile(token,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    },
    {
        method:'GET',
        path:'/user/showPeople/{token}',
        config:{
            description: 'showPeople',
            notes: 'showPeople',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.array().items(joi.object().keys({
                        _id: joi.any(),
                        email: joi.string().email(),
                        following:joi.array().items(),
                        followers:joi.array().items(),
                    }))
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token;
                user.showPeople(token,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    },
    {
        method:'PUT',
        path:'/user/followUnfollow/{token}/{email}/{option}',
        config:{
            description: 'follow/unfollow',
            notes: 'follow/unfollow',
            tags: ['api','user'],
            validate:{
                params:{
                    option:joi.string().valid('follow','unfollow').required(),
                    token:joi.string().required(),
                    email:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: {}
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token,
                    email=request.params.email,
                    option=request.params.option;
                user.followUnfollowPeople(token,email,option,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    }
    ,
    {
        method:'GET',
        path:'/user/showFollowers/{token}',
        config:{
            description: 'showFollowers',
            notes: 'showFollowers',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.array().items(joi.object().keys({
                        _id:joi.any(),
                        username:joi.string(),
                        email: joi.string().email(),
                    }))
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token;
                user.showFollowers(token,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    }
    ,
    {
        method:'GET',
        path:'/user/showFollowing/{token}',
        config:{
            description: 'showFollowing',
            notes: 'showFollowing',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.array().items(joi.object().keys({
                        _id:joi.any(),
                        username:joi.string(),
                        email: joi.string().email(),
                    }))
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token;
                user.showFollowing(token,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    }
    ,
    {
        method:'GET',
        path:'/user/searchUsers/{token}/{search}',
        config:{
            description: 'searchUsers',
            notes: 'searchUsers',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required(),
                    search:joi.string()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.array().items(joi.object().keys({
                        _id: joi.any(),
                        email: joi.string().email(),
                        following:joi.array().items(),
                        followers:joi.array().items(),
                    }))
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token,
                    search=request.params.search;
                user.searchUsers(token,search,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    }
    ,
    {
        method:'PUT',
        path:'/user/updateProfile/{token}/{options}/{value}',
        config:{
            description: 'updateOption',
            notes: 'updateOption',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required(),
                    options:joi.string().required().valid('password','email','username'),
                    value:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: {}
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                    var token=request.params.token,
                    options=request.params.options,
                    value=request.params.value;
                user.updateProfile(token,options,value,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            }

        }
    }
    ,
    {
        method:'PUT',
        path:'/user/activateDeactivate/{token}/{options}',
        config:{
            description: 'activateDeactivate',
            notes: 'activateDeactivate',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required(),
                    options:joi.string().required().valid('activate','deactivate'),
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: {}
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token,
                    options=request.params.options;
                user.activateDeactivate(token,options,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                });
            },


        }
    }
    ,
    {
        method:'PUT',
        path:'/user/infixToPostfix/{exp}',
        config:{
            description: 'infixToPostfix',
            notes: 'infixToPostfix',
            tags: ['api','user'],
            validate:{
                params:{
                    exp:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            handler: function(request, reply) {
                var exp=request.params.exp;
                reply(infixToPostfix(exp));
            },
            plugins: {
                'hapi-swagger': {
                    responses: CONSTANT.RESPONSEMESSAGES
                }
            },

        }
    },
    {
        method:'PUT',
        path:'/user/retweet/{token}/{tweetId}',
        config:{
            description: 'retweet',
            notes: 'retweet',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required(),
                    tweetId:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.object().keys({
                        retweetedBy: joi.array().items(),
                        tweet: joi.string(),
                        userId: joi.any(),
                        _id: joi.any(),
                        dateCreated:joi.date()
                    })
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token,
                    tweetId=request.params.tweetId;
                tweets.retweet(token,tweetId,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                })
            }

        }
    },
    {
        method:'GET',
        path:'/user/showUsersByDate/{token}/{from}/{to}',
        config:{
            description: 'show users by date',
            notes: 'show users by date',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required(),
                    from:joi.date().required().format("DD-MM-YYYY").description("DD-MM-YYYY"),
                    to:joi.date().min(joi.ref("from")).required().format("DD-MM-YYYY").description("DD-MM-YYYY")
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.array().items(joi.object().keys({
                        _id: joi.any(),
                        email: joi.string().email(),
                        following:joi.array().items(),
                        followers:joi.array().items(),
                    }))
                }
            },
            handler: function(request, reply) {
                var token=request.params.token,
                    from=request.params.from,
                    to=request.params.to;
                user.showUsersByDate(token,from,to,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                })
            }

        }
    },
    {
        method:'GET',
        path:'/user/showTweetsCountByDate/{token}/{from}/{to}',
        config:{
            description: 'show tweets Count by date',
            notes: 'show tweets Count by date',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required(),
                    from:joi.date().required().format("DD-MM-YYYY").description("DD-MM-YYYY"),
                    to:joi.date().min(joi.ref("from")).required().format("DD-MM-YYYY").description("DD-MM-YYYY")
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.number()
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token,
                    from=request.params.from,
                    to=request.params.to;
                user.showTweetsByDate(token,from,to,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                })
            }

        }
    },
    {
        method:'PUT',
        path:'/user/likeUnlike/{token}/{tweetId}/{option}',
        config:{
            description: 'likeUnlike',
            notes: 'likeUnlike',
            tags: ['api','user'],
            validate:{
                params:{
                    token:joi.string().required(),
                    option:joi.string().valid('like','unlike').required(),
                    tweetId:joi.string().required()
                },
                failAction:function(request,reply,source,error)
                {
                    var error=joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            response: {
                options: {
                    allowUnknown: true
                },
                schema: {
                    message: joi.string().required(),
                    data: joi.object().keys({
                        likedBy: joi.array().items(),
                        tweet: joi.string(),
                        userId: joi.any(),
                        _id: joi.any(),
                        dateCreated:joi.date()
                    })
                }
            },
            //plugins: {
            //    'hapi-swagger': {
            //        responses: CONSTANT.RESPONSEMESSAGES
            //    }
            //},
            handler: function(request, reply) {
                var token=request.params.token,
                    tweetId=request.params.tweetId,
                    option=request.params.option;
                tweets.likeUnlike(token,tweetId,option,function(err,result){
                    if(err)
                        reply(err.response).code(err.statusCode);
                    else
                        reply(result.response).code(result.statusCode);
                })
            }

        }
    },
    {
        method: 'POST',
        path: '/user/fileUpload',
        config: {
            description: 'fileUpload',
            notes: 'fileUpload',
            tags: ['api', 'user'],
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            payload: {
                output: 'stream',
                parse: true,
                allow: 'multipart/form-data'
            },
            validate: {
                payload: {
                    token: joi.string().required(),
                    file: joi.any().meta({swaggerType: 'file'}).description('input file').required()
                },
                failAction: function (request, reply, source, error) {
                    var error = joiError(error);
                    reply(error.response).code(error.statusCode);
                }
            },
            handler: function (request, reply) {
                var data = request.payload,
                    token = request.payload.token;

            user.fileUpload(token,data,function(err,result){
                if(err)
                    reply(err.response).code(err.statusCode);
                else
                    reply(result.response).code(result.statusCode);
            })

        }
    }}

];
//function fun(reply)
//{
//    if(err)
//        reply(err.response).code(err.statusCode);
//    else
//        reply(result.response).code(result.statusCode);
//}
