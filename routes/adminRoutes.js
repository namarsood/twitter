/**
 * Created by namarsood on 1/29/2016.
 */
var joi=require('joi'),
    user=require('../controller/user'),
    queries=require('../service/queries'),
    admin=require('../controller/admin'),
    joiError=require('../controller/joiError');

module.exports=[
{
    method:'DELETE',
        path:'/admin/deleteAccount/{email}/{token}',
    config:{
    description: 'deleteAccount',
        notes: 'deleteAccount',
        tags: ['api','admin'],
        validate: {
        params:{
            email:joi.string().required(),
                token:joi.string().required()
        },
            failAction:function(request,reply,source,error)
            {
                var error=joiError(error);
                reply(error.response).code(error.statusCode);
            }
    },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data: {}
            }
        },
        //plugins: {
        //    'hapi-swagger': {
        //        responses: CONSTANT.RESPONSEMESSAGES
        //    }
        //},
    handler: function(request, reply) {
        var email=request.params.email,
            token=request.params.token;
        admin.deleteAccount(token,email,function(err,result){
            if(err)
                reply(err.response).code(err.statusCode);
            else
                reply(result.response).code(result.statusCode);
        });
    }

}
}
,
{
    method:'POST',
        path:'/admin/Login',
    config:{
    description: 'Login',
        notes: 'Login',
        tags: ['api','admin'],
        validate:{
        payload:{
            email:joi.string().required(),
                password:joi.string().required()
        },
            failAction:function(request,reply,source,error)
            {
                var error=joiError(error);
                reply(error.response).code(error.statusCode);
            }
    },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data: {}
            }
        },
        //plugins: {
        //    'hapi-swagger': {
        //        responses: CONSTANT.RESPONSEMESSAGES
        //    }
        //},
    handler: function(request, reply) {
        var email=request.payload.email,
            password=request.payload.password;
        admin.login(password,email,function(err,result){
            if(err)
                reply(err.response).code(err.statusCode);
            else
                reply(result.response).code(result.statusCode);
        });
    }

}
},
{
    method:'DELETE',
        path:'/admin/Logout/{token}',
    config:{
    description: 'Logout',
        notes: 'Logout',
        tags: ['api','admin'],
        validate:{
        params:{
            token:joi.string().required()
        },
            failAction:function(request,reply,source,error)
            {
                var error=joiError(error);
                reply(error.response).code(error.statusCode);
            }
    },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data: {}
            }
        },
        //plugins: {
        //    'hapi-swagger': {
        //        responses: CONSTANT.RESPONSEMESSAGES
        //    }
        //},
    handler: function(request, reply) {
        var token=request.params.token;
        admin.logout(token,function(err,result){
            if(err)
                reply(err.response).code(err.statusCode);
            else
                reply(result.response).code(result.statusCode);
        })
    }

}
}
,
{
    method:'GET',
        path:'/admin/showUsers/{token}',
    config:{
    description: 'showUsers',
        notes: 'showUsers',
        tags: ['api','admin'],
        validate:{
        params:{
            token:joi.string().required()
        },
            failAction:function(request,reply,source,error)
            {
                var error=joiError(error);
                reply(error.response).code(error.statusCode);
            }
    },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data: joi.array().items(joi.object().keys({
                    _id: joi.any(),
                    email: joi.string().email(),
                    following:joi.array().items(),
                    followers:joi.array().items(),
                }))
            }
        },
        //plugins: {
        //    'hapi-swagger': {
        //        responses: CONSTANT.RESPONSEMESSAGES
        //    }
        //},
    handler: function(request, reply) {
        var token=request.params.token;
        admin.showUsers(token,function(err,result){
            if(err)
                reply(err.response).code(err.statusCode);
            else
                reply(result.response).code(result.statusCode);
        })
    }

}
}
,
{
    method:'GET',
        path:'/admin/showTweets/{token}',
    config:{
    description: 'showTweets',
        notes: 'showTweets',
        tags: ['api','admin'],
        validate:{
        params:{
            token:joi.string().required()
        },
            failAction:function(request,reply,source,error)
            {
                var error=joiError(error);
                reply(error.response).code(error.statusCode);
            }
    },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data:joi.array().items(joi.object().keys({
        _id: joi.any(),
        retweetedBy: joi.any(),
        tweet: joi.string(),
        userInfo: joi.object().keys({
            username: joi.string(),
            "email": joi.string().email()
        }),
        dateCreated: joi.date()
    }))}
        },

        //plugins: {
        //    'hapi-swagger': {
        //        responses: CONSTANT.RESPONSEMESSAGES
        //    }
        //},
    handler: function(request, reply) {
        var token=request.params.token;
        admin.showTweets(token,function(err,result){
            if(err)
                reply(err.response).code(err.statusCode);
            else
                reply(result.response).code(result.statusCode);
        })
    }

}
}
,
{
    method:'DELETE',
        path:'/admin/deleteTweet/{token}/{tweetId}',
    config:{
    description: 'deleteTweet',
        notes: 'deleteTweet',
        tags: ['api','admin'],
        validate:{
        params:{
            token:joi.string().required(),
                tweetId:joi.string().required()
        },
            failAction:function(request,reply,source,error)
            {
                var error=joiError(error);
                reply(error.response).code(error.statusCode);
            }
    },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data: {}
            }
        },
        //plugins: {
        //    'hapi-swagger': {
        //        responses: CONSTANT.RESPONSEMESSAGES
        //    }
        //},
    handler: function(request, reply) {
        var token=request.params.token,
            tweetId=request.params.tweetId;
        admin.deleteTweet(token,tweetId,function(err,result){
            if(err)
                reply(err.response).code(err.statusCode);
            else
                reply(result.response).code(result.statusCode);
        })
    }

}
},
{
    method:'POST',
        path:'/admin/registerAdmin',
    config:{
    description: 'registerAdmin',
        notes: 'registerAdmin',
        tags: ['api'],
        validate:{
        payload:{
            password:joi.string().required(),
                email:joi.string().email().required()
        },
            failAction:function(request,reply,source,error)
            {
                var error=joiError(error);
                reply(error.response).code(error.statusCode);
            }
    },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data: {}
            }
        },
        //plugins: {
        //    'hapi-swagger': {
        //        responses: CONSTANT.RESPONSEMESSAGES
        //    }
        //},
    handler:function(request,reply){
        var password=request.payload.password,
            email=request.payload.email;
        admin.register(password,email,function(err,result){
            if(err)
                reply(err.response).code(err.statusCode);
            else
                reply(result.response).code(result.statusCode);
        });

    }
}
},
{
    method:'PUT',
    path:'/admin/updateUserProfile/{token}/{options}/{value}/{email}',
    config:{
        description: 'updateOption',
        notes: 'updateOption',
        tags: ['api','user'],
        validate:{
            params:{
                token:joi.string().required(),
                options:joi.string().required().valid('password','email','username'),
                email:joi.string().email().required(),
                value:joi.string().required()
            },
            failAction:function(request,reply,source,error)
            {
                var error=joiError(error);
                reply(error.response).code(error.statusCode);
            }
        },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: joi.string().required(),
                data: {}
            }
        },
        //plugins: {
        //    'hapi-swagger': {
        //        responses: CONSTANT.RESPONSEMESSAGES
        //    }
        //},
        handler: function(request, reply) {
            var token=request.params.token,
                options=request.params.options,
                value=request.params.value,
                email=request.params.email;
            admin.updateUserProfile(token,options,value,email,function(err,result){
                if(err)
                    reply(err.response).code(err.statusCode);
                else
                    reply(result.response).code(result.statusCode);
            });
        }

    }
}];