/**
 * Created by namarsood on 1/20/2016.
 */
module.exports={
        EMAIL:'namarsood194@gmail.com',
        PASSWORD:'',
        ACCOUTNNAME:'namarsood',
        PRIVATEKEY:'shbsytrshh',
        HOST:'localhost',
        PORT:process.env.PORT||3008,
        STATUS_CODES:{
                OK:200,
                CREATED:201,
                DO_NOT_PROCESS:204,
                BAD_REQUEST:400,
                UNAUTHORIZED:401,
                PAYMENT_FAILURE:402,
                FORBIDDEN:403,
                NOT_FOUND:404,
                ALREADY_EXISTS_CONFLICT:409,
                UNSUPPORTED_MEDIA_TYPE:415,
                SERVER_ERROR:500
        },
        MESSAGES:{
                OK:"SUCESSFULL",
                CREATED:"CREATED",
                BAD_REQUEST:"BAD REQUEST",
                UNAUTHORIZED:"YOU ARE NOT AUTHORIZED",
                NOT_FOUND:"PAGE NOT FOUND",
                ALREADY_EXISTS_CONFLICT:"NOT UNIQUE",
        },
        RESPONSEMESSAGES:{
                "200":{description:"SUCESSFULL"},
                "201":{description:"CREATED"},
                "400":{description:"BAD_REQUEST"},
                "401":{description:"UNAUTHORIZED"},
                "404":{description:"NOT_FOUND"},
                "409":{description:"ALREADY_EXISTS_CONFLICT"}
        }
};
