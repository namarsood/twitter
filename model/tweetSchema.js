/**
 * Created by namarsood on 1/24/2016.
 */
var mongoose=require('mongoose'),
    schema=mongoose.Schema;
var user=require('./userSchema');

// tweet schema
var tweetSchema=new schema({
    userInfo:{type:schema.Types.ObjectId,ref:'user'},
    tweet:{type:String},
    dateCreated:{type:Date,default:new Date()},
    isActivated:{type:Boolean,default:true},
    isDeleted:{type:Boolean,default:false},
    retweetedBy:[{type:schema.Types.ObjectId}],
    likedBy:[{type:schema.Types.ObjectId}]

});
module.exports=mongoose.model('tweet',tweetSchema);