/**
 * Created by namarsood on 1/28/2016.
 */
var validator=require('validator');
var mongoose=require('mongoose'),
    schema=mongoose.Schema;


//admin schema
var adminSchema=new schema({
    email:{type:String,required:true,unique:true,validate:[validator.isEmail,'invalid email']},
    password:{type:String,required:true},
    token:{type:String,unique:true},
    isVerified:{type:String,default:true}
});
module.exports=mongoose.model('admin',adminSchema);