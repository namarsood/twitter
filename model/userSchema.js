/**
 * Created by namarsood on 1/22/2016.
 */
var validator=require('validator');
var mongoose=require('mongoose'),
    schema=mongoose.Schema;


//user schema
var userSchema=new schema({
    username:{type:String,required:true},
    profilePic:{type:String},
    dateRegistered:{type:Date,default:new Date()},
    email:{type:String,required:true,unique:true,validate:[validator.isEmail,'invalid email']},
    password:{type:String,required:true},
    followers:[{type:schema.Types.ObjectId}],
    following:[{type:schema.Types.ObjectId}],
    isVerified:{type:Boolean,default:true},
    token:{type:String,unique:true},
    isActivated:{type:Boolean,default:true},
    isDeleted:{type:Boolean,default:false}
});
 module.exports=mongoose.model('user',userSchema);
//validating email using validator.js
//1. install validator
//2. require it:  var validator = require('validator');
//3. use it like this
//var EmailSchema = new Schema({
//    email: {
//        //... other setup
//        validate: [ validator.isEmail, 'invalid email' ]
//    }
//});

// second method is using regex.test() method.
// where regex is valid regular expression.