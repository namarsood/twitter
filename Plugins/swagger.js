/**
 * Created by namarsood on 3/3/16.
 */
var Vision = require('vision'),
    CONSTANT=require('../config/constants'),
    HapiSwagger = require('hapi-swagger'),
    Inert = require('inert'),
    Pack = require('../package');
// Plugins
const swaggerOptions = {
    info: {
        'title': 'Twiter',
        'version': Pack.version,
        'contact':{
            'name':CONSTANT.ACCOUTNNAME,
            'email':CONSTANT.EMAIL
        }
    },
};

exports.register=function(server,options,next){
    server.register(
        {
            register: HapiSwagger,
            options: swaggerOptions
        }, function (err) {
        if (err) {
            server.log(['error'], 'hapi-swagger load error: ' + err)
        }else{
            server.log(['start'], 'hapi-swagger interface loaded')
        }
    });
    next();
}
exports.register.attributes = {
    name: 'swagger-plugin'
};