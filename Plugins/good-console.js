var Good=require('good');
exports.register=function(server,options,next){
    "use strict";
    server.register({
        register:Good,
        options:{
            reporters:[{
                reporter:require('good-console'),
                events:{
                    response:'*',
                    error:'*',
                    log:'*'
                }
            }]
        }
    },function(err){
        if(err)
        {
            throw err;
        }
    });
    next();
}
exports.register.attributes={
    name:'good-console-plugin'
};