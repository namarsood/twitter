/**
 * Created by namarsood on 1/28/2016.
 */
var queries=require('../service/queries'),
    userModel=require('../model/userSchema'),
    tweetModel=require('../model/tweetSchema'),
    adminModel=require('../model/adminSchema'),
    CONSTANT=require('../config/constants'),
    generateResponseObject=require('./generateResponseObject'),
    async=require('async'),
    crypt=require('./crypt'),
    jwt=require('jsonwebtoken'),
    mail=require('./mail');


module.exports.deleteAccount=function(token,email,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback)
        {
            var query={
                token:token,
                isVerified:true
            }
            queries.getData(adminModel,query,{_id:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function deleteUserSchema(result,callback)
        {
            var update={
                isDeleted:true
            }
            var condition={
                email:email,
                isDeleted:false
            }
            queries.findAndUpdate(userModel,condition,update,{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        },
        // 3rd
        function deleteFromFollowingOfOthers(result,callback)
        {
            var followersArr=result.followers;
            queries.updateData(userModel,{_id:{$in:followersArr}},{$pull:{following:result._id}},{lean:true,multi:true},function(err,result1){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result1.nModified)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        },
        // 4th
        function deleteFromFollowersOfOthers(result,callback)
        {
            var followingArr=result.following;
            queries.updateData(userModel,{_id:{$in:followingArr}},{$pull:{followers:result._id}},{lean:true,multi:true},function(err,result1){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result1.nModified)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        },
        // 5th
        function deleteTweetSchema(result,callback)
        {
            var update={
                isDeleted:true
            }
            var condition={
                userInfo:result._id
            }
            queries.updateData(tweetModel,condition,update,{lean:true,multi:true},function(err,result){
                if(err)
                    callback(err);
                else
                    callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
            })
        }
    ],function(err,result)
    {
        if(err)
        callback(err);
        else
        callback(result);
    })
}
module.exports.login=function(password,email,callback) {
    var conditions = {
        email: email,
        password:crypt.encrypt(password),
        isVerified:true
    }
    var tokenData = {
        email: email
    }
    var token= jwt.sign(tokenData, CONSTANT.PRIVATEKEY);
    var update={
        $set: {token:token}
    }
    queries.updateData(adminModel, conditions,update , {lean: true}, function (err, result) {
        if (err)
            callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
        else if (result.nModified)
            callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{"token":token},CONSTANT.STATUS_CODES.OK));
        else
            callback(null,generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
    });
}
module.exports.logout=function(token,callback)
{
    var conditions = {
        token:token,
        isVerified:true
    }
    var update={
        $unset: {token:""}
    }
    queries.updateData(adminModel, conditions,update ,{lean: true}, function (err, result) {
        if (err)
            callback(err);
        else if (result.nModified)
            callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
        else
            callback(null,generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
    });
}
module.exports.register=function(password,email,callback)
{
    var token=jwt.sign(email,CONSTANT.PRIVATEKEY);
    var data={
        password:crypt.encrypt(password),
        email:email,
        token:token
    }
    queries.saveData(adminModel,data,function(err,result){
        if(err)
            callback(generateResponseObject(CONSTANT.MESSAGES.ALREADY_EXISTS_CONFLICT,{},CONSTANT.STATUS_CODES.ALREADY_EXISTS_CONFLICT));
        else
        {
            console.log(result);
            var from = CONSTANT.ACCOUTNNAME+" Team<" + CONSTANT.EMAIL + ">";
            var mailbody = "<p>Thanks for Registering on "+CONSTANT.ACCOUTNNAME+" </p><p>Please verify your email by clicking on the verification link below.<br/><a href='http://"+CONSTANT.HOST+":"+CONSTANT.PORT+"/"+"verifyEmail"+"/"+token+adminModel+"'>Verification Link</a></p>"
            mail(from, email , "Account Verification", mailbody);
            callback(null,generateResponseObject("please confirm your id by clicking on link in email",{},CONSTANT.STATUS_CODES.CREATED))
        }
    })
}
module.exports.showTweets=function(token,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback)
        {
            var query={
                token:token,
                isVerified:true
            }
            queries.getData(adminModel,query,{_id:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function(result,callback)
        {
            queries.join(tweetModel,{isActivated:true,isDeleted:false},{ __v: 0},{lean:true,sort:{dateCreated:-1}},
                {path: 'userInfo', select: 'username email -_id'},function (err, result) {
                    if (err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if (result.length)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                });
        },

    ],function(err,result)
    {
        if(err)
            callback(err);
        else
            callback(result);
    })
}
module.exports.deleteTweet=function(token,tweetId,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback)
        {
            var query={
                token:token,
                isVerified:true
            }
            queries.getData(adminModel,query,{_id:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function deleteTweet(result,callback)
        {
            queries.updateData(tweetModel,{_id:tweetId},{isDeleted:true},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.nModified)
                    callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        }
    ],function(err,result)
    {
        if(err)
            callback(err);
        else
            callback(result);
    })
}
module.exports.showUsers=function(token,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback)
        {
            var query={
                token:token,
                isVerified:true
            }
            queries.getData(adminModel,query,{_id:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function getUsers(result,callback)
        {
            queries.getData(userModel,{},{__v:0},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        }
    ],function(err,result)
    {
        if(err)
            callback(err);
        else
            callback(result);
    })
}
module.exports.updateUserProfile=function(token,options,value,email,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback)
        {
            var query={
                token:token,
                isVerified:true
            }
            queries.getData(adminModel,query,{_id:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function updateUserProfile(result,callback)
        {
            var conditions={
                email:email,
                isActivated:true,
                isDeleted:false
            }
            if(options=="password")
            {
                var update={
                    password:crypt.encrypt(value)
                }

                queries.findAndUpdate(userModel,conditions,update,{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                })
            }
            else if(options=="username")
            {
                var update={
                    username:value
                }

                queries.findAndUpdate(userModel,conditions,update,{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                })
            }
            else
            {
                var update={
                    email:value
                }

                queries.findAndUpdate(userModel,conditions,update,{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                })
            }
        }
    ],function(err,result)
    {
        if(err)
            callback(err);
        else
            callback(result);
    })

}
