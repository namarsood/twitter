/**
 * Created by namarsood on 1/28/2016.
 */
module.exports=function(message,data,statusCode)
{
    return {
        response:{
            message:message,
                data:data
        },

        statusCode:statusCode
    }
}