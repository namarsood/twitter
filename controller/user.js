/**
 * Created by namarsood on 1/21/2016.
 */
var fs=require('fs');
var mail=require('./mail');
var userModel=require('../model/userSchema');
var queries=require('../service/queries');
var crypt=require('./crypt');
var jwt=require('jsonwebtoken');
var CONSTANT=require('../config/constants');
var async=require('async');
var generateResponseObject=require('./generateResponseObject');
var tweetModel=require('../model/tweetSchema');

module.exports.register=function(username,password,email,callback)
{
            var token=jwt.sign(email,CONSTANT.PRIVATEKEY);
            var data={
                username:username,
                password:crypt.encrypt(password),
                email:email,
                token:token
            }
            queries.saveData(userModel,data,function(err,result){
                if(err)
                callback(generateResponseObject(CONSTANT.MESSAGES.ALREADY_EXISTS_CONFLICT,{},CONSTANT.STATUS_CODES.ALREADY_EXISTS_CONFLICT));
                else
                {
                    console.log(result);
                    var from = CONSTANT.ACCOUTNNAME+" Team<" + CONSTANT.EMAIL + ">";
                    var mailbody = "<p>Thanks for Registering on "+CONSTANT.ACCOUTNNAME+" </p><p>Please verify your email by clicking on the verification link below.<br/><a href='http://"+"54.173.40.155"+":"+CONSTANT.PORT+"/"+"verifyEmail"+"/"+token+"'>Verification Link</a></p>"
                    mail(from, email , "Account Verification", mailbody);
                    callback(null,generateResponseObject("please confirm your id by clicking on link in email",{},CONSTANT.STATUS_CODES.CREATED))
                }
            })
}

module.exports.login=function(password,email,callback) {
    var conditions = {
        email: email,
        isDeleted:false,
        password:crypt.encrypt(password)
    }
    var tokenData = {
        email: email
    }
    var token=jwt.sign(tokenData, CONSTANT.PRIVATEKEY);
    var update={
    $set: {token: token}
    }
    queries.findAndUpdate(userModel, conditions,update , {lean: true}, function (err, result) {
        if (err)
        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
        else if (result)
        {
            result["token"]=token;
            callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
        }
        else
        callback(null,generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
    });
}
module.exports.logout=function(token,callback)
{
    var conditions = {
        token:token
    }
    var update={
    $unset: {token:""}
    }
    queries.updateData(userModel, conditions,update ,{lean: true}, function (err, result) {
        if (err)
        callback(err);
        else if (result.nModified)
        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
        else
        callback(null,generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
    });
}
module.exports.updateProfile=function(token,options,value,callback)
{
    var conditions={
        token:token,
        isActivated:true
    }
    if(options=="password")
    {
        var update={
            password:crypt.encrypt(value)
        }

        queries.findAndUpdate(userModel,conditions,update,{lean:true},function(err,result){
            if(err)
                callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            else if(result)
                callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
            else
                callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
        })
    }
    else if(options=="username")
    {
        var update={
            username:value
        }

        queries.findAndUpdate(userModel,conditions,update,{lean:true},function(err,result){
            if(err)
                callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            else if(result)
                callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
            else
                callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
        })
    }
    else
    {
        var update={
            email:value
        }

        queries.findAndUpdate(userModel,conditions,update,{lean:true},function(err,result){
            if(err)
                callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            else if(result)
                callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
            else
                callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
        })
    }
}
module.exports.activateDeactivate=function(token,options,callback)
{
    if(options=="activate")
    {
        var update={
            isActivated:true
        }
        async.waterfall([
            function updateUserSchema(callback)
            {
                var conditions={
                    token:token
                }
                queries.findAndUpdate(userModel,conditions,update,{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result)
                        callback(null,result);
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
                })
            },
            function updateTweetSchema(result,callback)
            {
                var conditions={
                    userInfo:result._id
                }

                queries.updateData(tweetModel,conditions,update,{lean:true,multi:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result.nModified)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{isActivated:true},CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject("Already Activated",{},CONSTANT.STATUS_CODES.ALREADY_EXISTS_CONFLICT));
                })
            }

        ],function(err,result){
            if(err)
                callback(err);
            else
                callback(err,result);
        });

    }
    else
    {
        var update={
            isActivated:false
        }
        async.waterfall([
            function updateUserSchema(callback)
            {
                var conditions={
                    token:token
                }
                queries.findAndUpdate(userModel,conditions,update,{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result)
                        callback(null,result);
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
                })
            },
            function updateTweetSchema(result,callback)
            {
                var conditions={
                    userInfo:result._id
                }

                queries.updateData(tweetModel,conditions,update,{lean:true,multi:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result.nModified)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{isActivated:false},CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject("Already Dectivated",{},CONSTANT.STATUS_CODES.ALREADY_EXISTS_CONFLICT));
                })
            }

        ],function(err,result){
            if(err)
                callback(err);
            else
                callback(err,result);
        });
    }
}

module.exports.showPeople=function(token,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback){
            var query={
                token:token,
                isActivated:true,
                isDeleted:false
            }
            queries.getData(userModel,query,{email:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function getPeople(result,callback)
        {
            queries.getData(userModel,{isDeleted:false},{email:1,followers:1,following:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        }
    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(err,result);
    })
}
module.exports.showFollowers=function(token,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback){
            var query={
                token:token,
                isActivated:true,
                isDeleted:false
            }
            queries.getData(userModel,query,{email:1,followers:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function getFollowers(result,callback)
        {
            var followersArr=result[0].followers;
            queries.getData(userModel,{_id:{$in:followersArr},isDeleted:false},{email:1,username:1},{},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,generateResponseObject("you have "+ followersArr.length +" followers",result,CONSTANT.STATUS_CODES.OK));
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        }
    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(err,result);
    })

}
module.exports.showFollowing=function(token,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback){
            var query={
                token:token,
                isActivated:true,
                isDeleted:false
            }
            queries.getData(userModel,query,{email:1,following:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function getFollowers(result,callback)
        {
            var followingArr=result[0].following;
            queries.getData(userModel,{_id:{$in:followingArr},isDeleted:false},{email:1,username:1},{},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,generateResponseObject("you are following "+ followingArr.length +" people",result,CONSTANT.STATUS_CODES.OK));
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        }
    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(err,result);
    })
}
module.exports.followUnfollowPeople=function(token,email,option,callback)
{
    if(option=='follow')
    {
        async.waterfall([
            // 1st
            function verifyToken(callback){
                var query={
                    token:token,
                    isActivated:true,
                    isDeleted:false
                }
                queries.getData(userModel,query,{email:1},{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result.length)
                        callback(null,result);
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
                })
            },
            // 2nd
            function addFollower(result,callback)
            {
                var conditions={
                    email:email,
                    isActivated:true,
                    isDeleted:false
                }

                var update={
                    $addToSet:{followers:result[0]._id}
                }
                queries.findAndUpdate(userModel,conditions,update,{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result)
                        callback(null,result);
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                })
            },
            // 3rd
            function addFollowing(result,callback)
            {
                var update={
                    $addToSet:{following:result._id}
                }
                var condition={
                    token:token
                }
                queries.updateData(userModel,condition,update,{lean:true},function(err,result){
                    if(err)
                        callback(err);
                    else if(result.nModified)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                })
            }
        ],function(err,result){
            if(err)
                callback(err);
            else
                callback(err,result);
        })

    }
    else
    {
        async.waterfall([
            // 1st
            function verifyToken(callback){
                var query={
                    token:token,
                    isActivated:true,
                    isDeleted:false
                }
                queries.getData(userModel,query,{_id:1},{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result.length)
                        callback(null,{'userId':result[0]._id});
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
                })
            },
            // 2nd
            function checkIfUserExist(result1,callback)
            {
                var query={
                    email:email,
                    isDeleted:false
                }
                queries.getData(userModel,query,{_id:1},{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result.length)
                    {
                        result1["followingId"]= result[0]._id;
                        callback(null,result1);
                    }

                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                })
            },
            // 3rd
            function removeFollowing(result2,callback)
            {
                var update={ $pull: { following: result2.followingId } };
                var condition={
                    _id:result2.userId,
                    isDeleted:false
                }
                queries.updateData(userModel,condition,update,{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result.nModified)
                        callback(null,result2);
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                })
            },
            // 4th
            function removeFollower(result3,callback)
            {
                var update={ $pull: { followers: result3.userId } };
                var condition={
                    _id:result3.followingId,
                    isDeleted:false
                }
                queries.updateData(userModel,condition,update,{lean:true},function(err,result){
                    if(err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if(result.nModified)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                })
            }
        ],function(err,result){
            if(err)
                callback(err);
            else
                callback(err,result);
        })
    }

}
module.exports.searchUsers=function(token,search,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback){
            var query={
                token:token,
                isActivated:true,
                isDeleted:false
            }
            queries.getData(userModel,query,{_id:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,{'userId':result[0]._id});
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function getPeople(result,callback)
        {
            queries.getData(userModel,{email:new RegExp(search,'i'),isDeleted:false},{email:1,followers:1,following:1},{lean:true},function(err,result){
                if(err)
                    callback(err);
                else if(result.length)
                    callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        }
    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(err,result);
    })
}
module.exports.showUsersByDate=function(token,from,to,callback)
{
    //from=new Date(from);
    //to=new Date(to);
    async.waterfall([
        // 1st
        function verifyToken(callback){
            var query={
                token:token,
                isActivated:true,
                isDeleted:false,
            }
            queries.getData(userModel,query,{email:1},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        function getPeople(result,callback)
        {
            queries.getData(userModel,{dateRegistered : { "$gte" : from, "$lte" : to },isDeleted:false},{email:1,followers:1,following:1},{lean:true},function(err,result){
                if(err)
                    callback(err);
                else if(result.length)
                    callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        }

    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(err,result);
    })
}
module.exports.showTweetsByDate=function(token,from,to,callback)
{
    //from=new Date(from);
    //to=new Date(to);
    async.waterfall([
        // 1st
        function verifyToken(callback){
            var query={
                token:token,
                isActivated:true,
                isDeleted:false,
            }
            queries.getData(userModel,query,{},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        function getTweetsCount(result,callback)
        {
            queries.countData(tweetModel,{dateCreated : { "$gte" : from, "$lte" : to },isDeleted:false,userInfo:result[0]._id},function(err,result){
                if(err)
                    callback(err);
                else if(result)
                    callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        }

    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(err,result);
    })
}
module.exports.fileUpload=function(token,data,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback){
            var query={
                token:token,
                isActivated:true,
                isDeleted:false,
            }
            queries.getData(userModel,query,{},{lean:true},function(err,result){
                if(err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        function fileUpload(result,callback)
        {
            if (data.file.hapi.headers['content-type'].split('/')[0]='image') {
                var name = data.file.hapi.filename;
                var path = __dirname + "/uploads/" + name;
                var file = fs.createWriteStream(path);

                file.on('error', function (err) {
                    console.log("err1");
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                });

                data.file.pipe(file);

                data.file.on('end', function (err) {
                    var ret = {
                        filename: data.file.hapi.filename,
                        headers: data.file.hapi.headers
                    }
                    //console.log("err1")
                    queries.updateData(userModel,{token:token,isDeleted:false},{$set:{profilePic:path}},function(err,result){
                        if(err)
                        {
                            //console.log("err2")
                            callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                        }
                        else if(result.nModified)
                        {
                            callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
                        }
                        else{
                            //console.log("err3")
                            callback(generateResponseObject("file with same name already exists",result.nModified,CONSTANT.STATUS_CODES.BAD_REQUEST));
                        }
                    })
                })
            }
        }

    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(err,result);
    })
}
module.exports.showPic=function(callback)
{
    async.waterfall([
        function showPic(callback)
        {

        }

    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(err,result);
    })
}
