/**
 * Created by namarsood on 1/21/2016.
 */
var queries=require('../service/queries');
var userModel=require('../model/userSchema');
var CONSTANT=require('../config/constants'),
    generateResponseObject=require('./generateResponseObject');
module.exports.verifyToken=function(token,model,callback)
{
    var condition={
        token:token
    }
    var update={
    $set:{isVerified:true}
    }
    queries.findAndUpdate(model,condition,update,{lean:true},function(err,result){
        if(err)
        callback(err);
        else if(!result)
        callback("<h1>invalid verification link");
        else if(result.isVerified===true)
        callback("<h1>account is already verified")
        else
        callback(null,'<h1>congrats email successfully verified<br/><h3>click on link below to go back to website<br/><a href="http://54.173.40.155:3008/documentation">click here</a>');
    });
}
module.exports.getToken=function(email,callback)
{
    var query={
        email:email,
        isDeleted:false
    }
    queries.getData(userModel,query,{token:1,_id:0},{lean:true},function(err,result){
        if(err)
        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
        else if(result.length)
        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{token:result[0].token},CONSTANT.STATUS_CODES.OK));
        else
        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
    });
}