/**
 * Created by namarsood on 1/21/2016.
 */
var queries=require('../service/queries'),
    userModel=require('../model/userSchema'),
    tweetModel=require('../model/tweetSchema'),
    async=require('async'),
    CONSTANT=require('../config/constants'),
    generateResponseObject=require('./generateResponseObject');

// Do tweets
module.exports.doTweets=function(tweet,token,callback)
{
    async.waterfall([
        // 1st
        function verifyToken(callback)
        {
            var query={
                token:token,
                isActivated:true,
                isDeleted:false
            }
            queries.getData(userModel,query,{email:1},{lean:true},function(err,result){
                if(err)
                callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result.length)
                callback(null,result);
                else
                callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            })
        },
        // 2nd
        function(result,callback)
        {
            var data={
                userInfo:result[0]._id,
                tweet:tweet
            }
            queries.saveData(tweetModel,data,function(err,result){
                if(err)
                callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if(result)
                callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{tweet:result.tweet,_id:result._id,dateCreated:result.dateCreated},CONSTANT.STATUS_CODES.OK));
                else
                callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            })
        }
    ],function(err,result){
        if(err)
        callback(err);
        else
        callback(null,result);
    })

}

// Timeline "show the tweets of logged in user + the people they are following"
module.exports.Timeline=function(token,callback) {
    async.waterfall([
        // 1st
        function verifyToken(callback)
        {
            var query = {
                token: token,
                isActivated:true,
                isDeleted:false
            }
            queries.getData(userModel, query, {}, {lean: true}, function (err, result) {
                //,select:'email',options:{lean:true}
                if (err)
                callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if (result.length)
                callback(null,result);
                else
                callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            });
        },
        // 2nd
        function(result,callback)
        {
            var arr = result[0].following;
            arr.push(result[0]._id);
            queries.join(tweetModel,{userInfo: {$in: arr},isActivated:true,isDeleted:false},{ __v: 0,isDeleted:0,isActivated:0},{lean:true,sort:{dateCreated:-1}},
                {path: 'userInfo', select: 'username email _id'},function (err, result) {
                    if (err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if (result.length)
                    callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
                    else
                    callback(generateResponseObject("no tweets to show",result,CONSTANT.STATUS_CODES.OK));
                });
        }
    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(null,result);
    })

}

// Profile "show tweets of the logged in user"
module.exports.Profile=function(token,callback) {
    async.waterfall([
        // 1st
        function verifyToken(callback)
        {
            var query = {
                token: token,
                isActivated:true,
                isDeleted:false
            }
            queries.getData(userModel, query, {}, {lean: true}, function (err, result) {
                if (err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if (result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            });
        },
        // 2nd
        function(result1,callback)
        {
            queries.join(tweetModel,{userInfo:result1[0]._id,isDeleted:false},{ __v: 0},{lean:true,sort:{dateCreated:-1}},
                {path: 'userInfo', select: 'username email _id'},function (err, result) {
                    if (err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));

                    else if (result.length)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
                    else
                        callback(null,generateResponseObject("no tweets to show",[{userInfo:{username:result1[0].username,_id:result1[0]._id,email:result1[0].email}}],CONSTANT.STATUS_CODES.OK));
                });
        }
    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(null,result);
    })
}
// RETWEET
module.exports.retweet=function(token,tweetId,callback) {
    //var userId;
    async.waterfall([
        // 1st
        function verifyToken(callback)
        {
            var query = {
                token: token,
                isActivated:true,
                isDeleted:false
            }
            queries.getData(userModel, query, {}, {lean: true}, function (err, result) {
                if (err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if (result.length)
                    callback(null,result);
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
            });
        },
        // 2nd
        function updateRetweetedByArray(result,callback)
        {
            var update={
                $addToSet:{retweetedBy:result[0]._id}
            }
            queries.findAndUpdate(tweetModel, {_id:tweetId},update, {lean: true}, function (err, result) {
                if (err)
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                else if (result)
                    callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{_id:result._id,userId:result.userInfo,dateCreated:result.dateCreated,tweet:result.tweet,retweetedBy:result.retweetedBy},CONSTANT.STATUS_CODES.OK));
                else
                    callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
            });
        },
        //// 3rd
        //function saveTweet(result,callback)
        //{
        //
        //    queries.updateData(tweetModel,);//{retweetedBy:result[0].userId,tweet:result[0].tweet,userInfo:result[0].userInfo,retweetedBy:userId},function(err,result){
        //    //    if(err)
        //    //        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
        //    //    else if(result)
        //    //        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,result,CONSTANT.STATUS_CODES.OK));
        //    //    else
        //    //        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
        //    //})
        //}
    ],function(err,result){
        if(err)
            callback(err);
        else
            callback(null,result);
    })
}
// LIKE UNLIKE
module.exports.likeUnlike=function(token,tweetId,option,callback) {
    if(option=='like')
    {
        async.waterfall([
            // 1st
            function verifyToken(callback)
            {
                var query = {
                    token: token,
                    isActivated:true,
                    isDeleted:false
                }
                queries.getData(userModel, query, {}, {lean: true}, function (err, result) {
                    if (err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if (result.length)
                        callback(null,result);
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
                });
            },
            // 2nd
            function updateLikedByArray(result,callback)
            {
                var update={
                    $addToSet:{likedBy:result[0]._id}
                }
                queries.findAndUpdate(tweetModel, {_id:tweetId},update, {lean: true}, function (err, result) {
                    if (err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if (result)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{_id:result._id,userId:result.userInfo,dateCreated:result.dateCreated,tweet:result.tweet,likedBy:result.likedBy},CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                });
            },
        ],function(err,result){
            if(err)
                callback(err);
            else
                callback(null,result);
        })
    }
    else
    {
        async.waterfall([
            // 1st
            function verifyToken(callback)
            {
                var query = {
                    token: token,
                    isActivated:true,
                    isDeleted:false
                }
                queries.getData(userModel, query, {}, {lean: true}, function (err, result) {
                    if (err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if (result.length)
                        callback(null,result);
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.UNAUTHORIZED,{},CONSTANT.STATUS_CODES.UNAUTHORIZED));
                });
            },
            // 2nd
            function updateLikedByArray(result,callback)
            {
                var update={ $pull: { likedBy: result[0]._id } };
                queries.findAndUpdate(tweetModel, {_id:tweetId},update, {lean: true}, function (err, result) {
                    if (err)
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                    else if (result)
                        callback(null,generateResponseObject(CONSTANT.MESSAGES.OK,{},CONSTANT.STATUS_CODES.OK));
                    else
                        callback(generateResponseObject(CONSTANT.MESSAGES.BAD_REQUEST,{},CONSTANT.STATUS_CODES.BAD_REQUEST));
                });
            },
        ],function(err,result){
            if(err)
                callback(err);
            else
                callback(null,result);
        })
    }
}