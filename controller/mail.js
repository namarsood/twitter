/**
 * Created by namarsood on 1/21/2016.
 */
var nodemailer = require("nodemailer");

// create reusable transport method (opens pool of SMTP connections)
var CONSTANT=require('../config/constants');
var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
        user: CONSTANT.EMAIL,
        pass: CONSTANT.PASSWORD
    }
});

module.exports=function(from, email, subject, htmlBody, textBody){
    var mailOptions = {
        from: from, // sender address
        to: email, // list of receivers
        subject: subject, // Subject line
        text: textBody, // plaintext body
        html: htmlBody  // html body
    };

    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.error(error);
        }
        smtpTransport.close(); // shut down the connection pool, no more messages
    });
}