/**
 * Created by namarsood on 2/17/2016.
 */
var location=require('../model/geoJsonSchema');
var queries=require('../service/queries');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/practice');

//queries.saveData(location,{loc:{coordinates : [100.409046, 38.783748]}},function(err,result){
//    if(err)
//    console.log(err);
//    else
//    console.log(result);
//});
//$geometry : {
//    type : "Point",
//        coordinates :
//},
//{


//    var query ={geometry:{
//        coordinates: {
//    $near: [-122.409046, 37.783748],
//        $maxDistance: 10000000000
//},
//    type:'Point'}}
//var query={loc:{
//    $near : {
//        $geometry : {
//            type : "Point",
//            coordinates : [100, 30]
//        },
//        $maxDistance : 100000000
//    }
//}}
//queries.getData(location,query,{},{lean:true},function(err,result)
//{
//    if(err)
//    console.log(err);
//    else
//    console.log(result[0].loc.coordinates);
//})
locations.aggregate([
    {
        $geoNear: {
            near: { type: "Point", coordinates: [ -73.99279 , 40.719296 ] },
            distanceField: "dist.calculated",
            maxDistance: 2,
            query: { type: "public" },
            includeLocs: "dist.location",
            num: 5,
            spherical: true
        }
    },function(err,result){
        if(err)
        console.log(err);
    else
    console.log(result);
    }
])
