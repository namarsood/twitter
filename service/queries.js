/**
 * Created by namarsood on 1/20/2016.
 */


// SAVE
exports.saveData=function(model,data,callback)
{
    new model(data).save(function(err,resultData){
        if(err)
        {
            console.log(err);
            return callback(err);
        }
        else
        {
            var result=resultData.toObject();
            delete result.__v;
            return callback(null,result);
        }
    })
}

// UPDATE
exports.updateData=function(model,conditions, update, options, callback)
{
    model.update(conditions, update, options, function(err,result){
        if(err)
            return callback(err);
        else
        {
            return callback(null,result);
        }
    })
}

// DELETE
exports.deleteData=function(model,conditions,callback)
{
    model.remove(conditions,function(err,result){
        if(err)
        {
            console.log(err);
            return callback(err);
        }
        else
        {
            return callback(null,result);
        }
    })
}

// RETRIEVE
exports.getData = function (model, query, projection, options, callback) {

    model.find(query, projection, options, function (err, data) {
        if (err) {
            console.log(err);
            return callback(err);
        }
        return callback(null, data);
    });
};

// COUNT
exports.countData = function (model, query, callback) {

    model.count(query, function (err, count) {
        if (err) {
            console.log(err);
            return callback(err);
        }
        return callback(null, count);
    });
};


// FIND AND UPDATE
exports.findAndUpdate=function(model,conditions,update,options,callback)
{
    model.findOneAndUpdate(conditions,update,options,function(err,result){
      if(err)
      callback(err);
      else
      callback(null,result);

    })
}

// POPULATE
exports.join = function (model, query, projection, options, collectionOptions, callback) {
    model.find(query, projection, options).populate(collectionOptions).exec(function (err, data) {

        if (err) {
            logger.error("Error Data reference: ", err);
            return callback(err);
        }
        return callback(null, data);

    });
};


// DELETE DATA
exports.deleteData = function (model, conditions, callback) {

    model.remove(conditions, function (err, removed) {

        if (err) {
            logger.error("Delete Data", err);
            return callback(err);
        }
        return callback(null, removed);


    });
};
////FIND USER BY ID
//exports.findById=function(model,id,callback){
//    model.findUserByIdAndUserName(id,function(err,result){
//    if(err)
//    return callback(err)
//    else
//    return callback(err,result);
//    });
//};



